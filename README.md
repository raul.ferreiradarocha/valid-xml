# XML validation
## An xml validation system for Brazilian invoices
![Valid XML](https://gitlab.com/raul.ferreiradarocha/valid-xml/-/raw/master/cover.png)

### Functional Requirements:
- [x] The system must have a screen to upload a file in the ".xml" extension
- [x] The system must validate that the file is an .xml extension
- [x] The system should only allow uploading of the xml file if the issuer's CNPJ field (<emit>) is "09066241000884"
- [x] The system must validate if the invoice has an authorization protocol filled in (field <nProt>)
- [x]  The system should display the following data on a screen: Invoice number, Invoice date, complete recipient data and total invoice amount

### Non-functional requirements:
- [x] The data that will be displayed on the screen must be stored in a MySQL database
- [x] Must be developed in PHP 7 language
- [x] No Framework

#### Instructions
- 1. Upload the files to your server or git clone in your folder
- 2. Create a table based on the ClassCadastro model in your mysql database
- 3. Change the database connection settings in config/config.php
- 4. To login change the basic authentication in ClassRender.php or you can login with login admin password admin.
- 5. Now just upload your files.
- 6. To view the note, simply enter the cpf or note number

#### References
- [Linkedin author](https://www.linkedin.com/in/raul-ferreira-da-rocha-65a869186/)
- [XML Manipulation](https://www.php.net/manual/en/refs.xml.php)
- [PHP](https://www.php.net/)
- [MySQL](https://www.mysql.com/doc/)
