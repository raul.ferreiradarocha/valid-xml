<?php

$PastaInterna = "valid-xml/";

define('DIRPAGE', 'http://' . $_SERVER["HTTP_HOST"] . '/' . $PastaInterna . '');

if (substr($_SERVER['DOCUMENT_ROOT'], -1) == '/') {
    $URL = "{$_SERVER['DOCUMENT_ROOT']}{$PastaInterna}";
} else {
    $URL = "{$_SERVER['DOCUMENT_ROOT']}/{$PastaInterna}";
}
define('DIRREQ', $URL);
define('DIRIMG', DIRPAGE . "public/img/");
define('DIRCSS', DIRPAGE . "public/css/");
define('DIRJS', DIRPAGE . "public/js/");
define('DIRXML', DIRREQ . "public/xml/");
define('PAGEXML', DIRPAGE . "public/xml/");

#Acesso ao banco de dados
define('HOST', "localhost");
define('DB', "sistema");
define('USER', "root");
define('PASS', "");
