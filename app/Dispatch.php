<?php

namespace App;


use Src\classes\ClassRoutes;

class Dispatch extends ClassRoutes
{

    #Atributos
    private $Method;
    private $Param = [];
    private $Obj;

    public function __construct()
    {
        self::addController();
    }

    /**
     * Método de adição de Controller
     */
    private function addController()
    {
        $RotaController = $this->getRota();
        $NameS = "App\\Controller\\" . $RotaController . "";
        $this->Obj = new $NameS;
        if (isset($this->parseUrl()[1])) {
            self::addMethod();
        }
    }

    /**
     * Método de adição de metodo do controller
     */
    private function addMethod()
    {
        if (method_exists($this->Obj, $this->parseUrl()[1])) {
            $this->setMethod("{$this->parseUrl()[1]}");
            self::addParam();
            call_user_func_array([$this->Obj, $this->getMethod()], $this->getParam());
        } else {
            echo "Esse Metodo nao existe";
        }
    }

    /**
     * Método de adição de parâmetros do controller
     */
    private function addParam()
    {

        $ContArray = count($this->parseUrl());
        if ($ContArray > 2) {
            foreach ($this->parseUrl() as $Key => $Value) {
                if ($Key > 1) {
                    $this->setParam($this->Param += [$Key => $Value]);
                }
            }
        }
    }

    protected function setMethod($Method)
    {
        $this->Method = $Method;
    }
    public function getMethod()
    {

        return $this->Method;
    }

    public function setParam($Param)
    {
        $this->Param = $Param;
    }
    protected function getParam()
    {
        return $this->Param;
    }
}
