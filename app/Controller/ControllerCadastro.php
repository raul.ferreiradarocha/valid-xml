<?php

namespace App\Controller;

use App\Model\ClassCadastro;
use Src\classes\ClassRender;


class ControllerCadastro extends ClassCadastro
{

    protected $xml;
    protected $CNPJ;
    protected $nProt;
    protected $numeroNF;
    protected $data_dhEmi;
    protected $dados_CPF;
    protected $dados_Nome;
    protected $dados_Lgr;
    protected $dados_Nro;
    protected $dados_Cpl;
    protected $dados_Bairro;
    protected $dados_cMun;
    protected $dados_xMun;
    protected $dados_UF;
    protected $dados_CEP;
    protected $dados_cPais;
    protected $dados_xPais;
    protected $dados_fone;
    protected $dados_indIEDest;
    protected $valor_total_nf;
    protected $dados;
    protected $render;




    public function __construct()
    {
        $this->render = new ClassRender();
        $this->render->setTitle("Cadastro");
        $this->render->setDescription("Faça seu cadastro.");
        $this->render->setKeywords("cadastro de clientes, cadastro");
        $this->render->setDir("cadastro");
        $this->render->renderLayout();
    }

    /**
     * Extrai os dados do XML
     *
     * @return array
     */
    private function extractData()
    {

        if (count($_FILES) > 0) {
            $target_dir = DIRXML;
            $dhs = md5(date("YmdHis"));
            $target_file = $target_dir . basename($_FILES["xml"]["name"]);
            $uploadOk = 1;

            $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            $target_file = $target_dir . basename($dhs . '.' . $fileType);
            $uploadOk = 1;
            if ($fileType == 'xml') {
                $uploadOk = 1;
                if (move_uploaded_file($_FILES["xml"]["tmp_name"], $target_file)) {
                    chmod($target_file, 0744);
                    $this->xml = $target_file;
                    $arquivo = file_get_contents($target_file);


                    $xml = simplexml_load_string($arquivo) or die("Error: Cannot create object");

                    $vars = urldecode(http_build_query($xml));

                    $dados['xml'] = $target_file;
                    $dados['cnpj'] = $this->CNPJ = $this->getDadosNota($vars, '[emit][CNPJ]');
                    if ($this->CNPJ == '09066241000884') {
                        //echo 'ok';
                    } else {
                        echo $this->render->alert('Este CNPJ não é autorizado!', 'alert');
                        chmod($target_file, 0777);
                        unlink($target_file);
                        exit;
                    }
                    $dados['nprot'] = $this->nProt = $this->getDadosNota($vars, 'protNFe[infProt][nProt]');
                    if (strlen($this->nProt) > 5 && is_numeric($this->nProt)) {
                    } else {

                        echo $this->render->alert('Este Protocolo não é autorizado!', 'alert');
                        chmod($target_file, 0777);
                        unlink($target_file);
                        exit;
                    }

                    $dados['numero_nf'] = $this->numeroNF = $this->getDadosNota($vars, '[nNF]');
                    $linha = $this->verificaSeExiste($dados['numero_nf']);
                    if ($linha > 0) {
                        echo $this->render->alert('Nota já Cadastrada!', 'alert');
                        chmod($target_file, 0777);
                        unlink($target_file);
                        exit;
                    }
                    $dados['data_dhmi'] = $this->data_dhEmi = $this->getDadosNota($vars, '[dhEmi]');
                    if (strlen($dados['data_dhmi']) > 25) {
                        echo $this->render->alert('Dados Incorretos!-Nota inválida!', 'alert');
                        chmod($target_file, 0777);
                        unlink($target_file);
                        exit;
                    }
                    $dados['dados_cpf'] = $this->dados_CPF = $this->getDadosNota($vars, '[dest][CPF]');



                    if (strlen($dados['dados_cpf']) > 14) {
                        echo $this->render->alert('Dados Incorretos!-Nota inválida!', 'alert');
                        chmod($target_file, 0777);
                        unlink($target_file);
                        exit;
                    }
                    $dados['dados_nome'] = $this->dados_Nome = $this->getDadosNota($vars, '[dest][xNome]');
                    $dados['dados_lgr'] = $this->dados_Lgr = $this->getDadosNota($vars, '[dest][enderDest][xLgr]');
                    $dados['dados_nro'] = $this->dados_Nro = $this->getDadosNota($vars, '[dest][enderDest][nro]');
                    $dados['dados_cpl'] = $this->dados_Cpl = $this->getDadosNota($vars, '[dest][enderDest][xCpl]');
                    $dados['dados_bairro'] = $this->dados_Bairro = $this->getDadosNota($vars, '[dest][enderDest][xBairro]');
                    $dados['dados_cmun'] = $this->dados_cMun = $this->getDadosNota($vars, '[dest][enderDest][cMun]');
                    $dados['dados_xmun'] = $this->dados_xMun = $this->getDadosNota($vars, '[dest][enderDest][xMun]');
                    $dados['dados_uf'] = $this->dados_UF = $this->getDadosNota($vars, '[dest][enderDest][UF]');
                    $dados['dados_cep'] = $this->dados_CEP = $this->getDadosNota($vars, '[dest][enderDest][CEP]');
                    $dados['dados_cpais'] = $this->dados_cPais = $this->getDadosNota($vars, '[dest][enderDest][cPais]');
                    $dados['dados_xpais'] = $this->dados_xPais = $this->getDadosNota($vars, '[dest][enderDest][xPais]');
                    $dados['dados_fone'] = $this->dados_fone = $this->getDadosNota($vars, '[dest][enderDest][fone]');
                    if (strlen($dados['dados_fone']) > 14) {
                        $dados['dados_fone'] = 'não informou';
                    }
                    $dados['dados_indiedest'] = $this->dados_indIEDest = $this->getDadosNota($vars, '[dest][indIEDest]');
                    if (strlen($dados['dados_indiedest']) > 3) {
                        $dados['dados_indiedest'] = 0;
                    }
                    $dados['valor_total_nf'] = $this->valor_total_nf = $this->getDadosNota($vars, '[ICMSTot][vNF]');

                    $this->dados = $dados;
                } else {
                    $uploadOk = 0;
                }
            } else {
                $uploadOk = 0;
            }
        }
        return $this->dados;
    }
    #pegar dados 1
    private function getDadosNota($vars, $string)
    {
        $posicao_Dados = strpos($vars, $string);
        $string_Dados = substr($vars, $posicao_Dados);
        $final_posicao_Dados = strpos($string_Dados, '&');
        $Dados = substr($string_Dados, 0, $final_posicao_Dados);
        $_Dados = explode("=", $Dados);
        $dados_Dados = $_Dados[1];
        return $dados_Dados;
    }

    #Chamar o método de cadastro da ClassCadastro
    public function cadastrar()
    {

        if (count($_FILES) > 0) {
            $this->extractData();
            $this->cadastroXML($this->dados);
            echo $this->render->alert('Cadastro Efetuado com Sucesso!', 'success');
        }
    }

    #Selecionar e exibir os dados do banco de dados
    public function seleciona()
    {


        $B = $this->selecionaClientes();
        //var_dump($B);
        if (!isset($B["Mensagem"])) {
            $linhasH = '<table><thead><tr>';
            for ($i = 0; $i < 1; $i++) {
                foreach ($B[$i] as $key => $val) {
                    $linhasH .= "<th>" . $key . "</th>";
                    if ($i > 0) {
                        $linhasH .= '</tr></thead>';
                        continue;
                    }
                }
            }
        }

        if (!isset($B["Mensagem"])) {
            $linhasB = '<tbody>';
            for ($i = 0; $i < count($B); $i++) {
                $linhasB .= '<tr>';
                foreach ($B[$i] as $key => $val) {
                    if ($key == 'Valor Total Nota Fiscal') {
                        $linhasB .= "<td>R$ " . str_replace(".", ",", $val) . "</td>";
                    } else {
                        $linhasB .= "<td>" . $val . "</td>";
                    }
                }
                $linhasB .= '</tr>';
            }
            $linhasB .= '</tbody></table>';
        }
        if (!isset($B["Mensagem"])) {
            $html = '<div class="container">';
            $html .= "<table>";
            $html .= "<thead><tr>";
            $html .= $linhasH;
            $html .= $linhasB;
            $html .= '</div>';
            echo $html;
        } else {

            echo $this->render->alert($B["Mensagem"], 'alert');
        }
    }
}
