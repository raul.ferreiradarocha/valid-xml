<?php

namespace App\Model;

use App\Model\ClassConexao;

class ClassCadastro extends ClassConexao
{
    private $Db;

    protected function cadastroXML($dados)
    {
        $columns =  "insert into teste values(";
        $columns .=  ":id, :xml, :cnpj, :nprot, :numero_nf, :data_dhmi,";
        $columns .=  ":dados_cpf, :dados_nome, :dados_lgr, :dados_nro, :dados_cpl, :dados_bairro,";
        $columns .=  ":dados_cmun, :dados_xmun, :dados_uf, :dados_cep, :dados_cpais, :dados_xpais, ";
        $columns .=  ":dados_fone, :dados_indiedest, :valor_total_nf)";

        $Id = 0;
        $this->Db = $this->conexaoDB()->prepare($columns);

        $this->Db->bindParam(":id", $Id, \PDO::PARAM_INT);
        $this->Db->bindParam(":xml", $dados['xml'], \PDO::PARAM_STR);
        $this->Db->bindParam(":cnpj", $dados['cnpj'], \PDO::PARAM_STR);
        $this->Db->bindParam(":nprot", $dados['nprot'], \PDO::PARAM_STR);
        $this->Db->bindParam(":numero_nf", $dados['numero_nf'], \PDO::PARAM_STR);
        $this->Db->bindParam(":data_dhmi", $dados['data_dhmi'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_cpf", $dados['dados_cpf'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_nome", $dados['dados_nome'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_lgr", $dados['dados_lgr'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_nro", $dados['dados_nro'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_cpl", $dados['dados_cpl'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_bairro", $dados['dados_bairro'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_cmun", $dados['dados_cmun'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_xmun", $dados['dados_xmun'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_uf", $dados['dados_uf'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_cep", $dados['dados_cep'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_cpais", $dados['dados_cpais'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_xpais", $dados['dados_xpais'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_fone", $dados['dados_fone'], \PDO::PARAM_STR);
        $this->Db->bindParam(":dados_indiedest", $dados['dados_indiedest'], \PDO::PARAM_STR);
        $this->Db->bindParam(":valor_total_nf", $dados['valor_total_nf'], \PDO::PARAM_STR);
        $INFO = $this->Db->execute();
        if (!$INFO) {
            echo "\nPDO::errorInfo():\n";
            print_r($this->Db->errorInfo());
        }
    }

    public function verificaSeExiste($numero_nf)
    {

        $BFetch = $this->Db = $this->conexaoDB()->prepare("select * from teste where numero_nf='$numero_nf'");
        $INFO = $BFetch->execute();
        if (!$INFO) {
            echo "\nPDO::errorInfo():\n";
            print_r($this->Db->errorInfo());
        }
        $rows = $BFetch->fetchAll();

        return count($rows);
    }


    #Acesso ao banco de dados com seleção
    protected function selecionaClientes()
    {


        (int)$numero_nf = '%' . $_POST['numero_nf'] . '%';
        if (strlen($numero_nf) > 0) {

            $BFetch = $this->Db = $this->conexaoDB()->prepare("select * from teste where numero_nf like '$numero_nf'");
        }
        (int)$dados_cpf = '%' . $_POST['dados_cpf'] . '%';
        if (strlen($dados_cpf) > 3) {

            $BFetch = $this->Db = $this->conexaoDB()->prepare("select * from teste where dados_cpf like '$dados_cpf'");
        }

        if (isset($BFetch)) {
        } else {
            return $Array = array("Mensagem" => "Não há Dados");
            exit;
        }
        $BFetch->bindParam(":id", $id, \PDO::PARAM_INT);
        $BFetch->bindParam(":xml", $xml, \PDO::PARAM_STR);
        $BFetch->bindParam(":cnpj", $cnpj, \PDO::PARAM_STR);
        $BFetch->bindParam(":nprot", $nprot, \PDO::PARAM_STR);
        $BFetch->bindParam(":numero_nf", $numero_nf, \PDO::PARAM_STR);
        $BFetch->bindParam(":data_dhmi", $data_dhmi, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_cpf", $dados_cpf, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_nome", $dados_nome, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_lgr", $dados_lgr, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_nro", $dados_nro, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_cpl", $dados_cpl, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_bairro", $dados_bairro, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_cmun", $dados_cmun, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_xmun", $dados_xmun, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_uf", $dados_uf, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_cep", $dados_cep, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_cpais", $dados_cpais, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_xpais", $dados_xpais, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_fone", $dados_fone, \PDO::PARAM_STR);
        $BFetch->bindParam(":dados_indiedest", $dados_indiedest, \PDO::PARAM_STR);
        $BFetch->bindParam(":valor_total_nf", $dados_indiedest, \PDO::PARAM_STR);
        $INFO = $BFetch->execute();
        if (!$INFO) {
            echo "\nPDO::errorInfo():\n";
            print_r($this->Db->errorInfo());
        }
        $I = 0;
        while ($Fetch = $BFetch->fetch(\PDO::FETCH_ASSOC)) {
            $Array[$I] = [

                'Numero NF' => $Fetch['numero_nf'],
                'Data de Emissão' => $Fetch['data_dhmi'],
                'CPF' => $Fetch['dados_cpf'],
                'Nome' => $Fetch['dados_nome'],
                'Logradouro' => $Fetch['dados_lgr'],
                'Nº' => $Fetch['dados_nro'],
                'Complemento' => $Fetch['dados_cpl'],
                'Bairro' => $Fetch['dados_bairro'],
                'Código Município' => $Fetch['dados_cmun'],
                'Município' => $Fetch['dados_xmun'],
                'UF' => $Fetch['dados_uf'],
                'Cep' => $Fetch['dados_cep'],
                'Código País' => $Fetch['dados_cpais'],
                'Fone' => $Fetch['dados_fone'],
                'indIeDest' => $Fetch['dados_indiedest'],
                'Valor Total Nota Fiscal' => $Fetch['valor_total_nf'],

            ];
            $I++;
        }
        if (count($Array) > 0) {
            return $Array;
        } else {
            return array("Mensagem" => "Não há Dados");
        }
    }
}
