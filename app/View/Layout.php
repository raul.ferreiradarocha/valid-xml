<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Webdesign em Foco">
    <meta name="description" content="<?php echo $this->getDescription(); ?>">
    <meta name="keywords" content="<?php echo $this->getKeywords(); ?>">
    <title><?php echo $this->getTitle(); ?></title>
    <link rel="stylesheet" href="<?php echo DIRCSS . 'Style.css' ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php echo $this->addHead(); ?>
</head>

<body>
    <div class="header">
        <a href="<?php echo DIRPAGE; ?>" class="logo">CompanyLogo</a>
        <div class="header-right">
            <a class="active" href="<?php echo DIRPAGE . 'cadastro'; ?>">Cadastro XML</a>
            <a href="#contact">Contato</a>
        </div>
    </div>
    <div class="Main">
        <?php echo $this->addMain(); ?>
    </div>

    <div class="Footer">

        <?php echo $this->addFooter(); ?>
    </div>
</body>

</html>