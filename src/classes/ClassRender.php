<?php
namespace Src\classes;
require_once('../config/config.php');


class ClassRender
{

    #Propriedades
    private $Dir;
    private $Title;
    private $Description;
    private $Keywords;

    public function getDir()
    {
        return $this->Dir;
    }
    public function setDir($Dir)
    {
        $this->Dir = $Dir;
    }
    public function getTitle()
    {
        return $this->Title;
    }
    public function setTitle($Title)
    {
        $this->Title = $Title;
    }
    public function getDescription()
    {
        return $this->Description;
    }
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }
    public function getKeywords()
    {
        return $this->Keywords;
    }
    public function setKeywords($Keywords)
    {
        $this->Keywords = $Keywords;
    }
    public function require_auth() {
        $AUTH_USER = 'admin';
        $AUTH_PASS = 'admin';
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }
    }
    #Método responsável por renderizar todos o layout
    public function renderLayout()
    {
       $this->require_auth();
       include_once(DIRREQ . "app/View/Layout.php");
    }

    #Adiciona características específicas no head
    public function addHead()
    {

        if (file_exists(DIRREQ . "app/View/{$this->getDir()}/Head.php")) {
            
            include(DIRREQ . "app/View/{$this->getDir()}/Head.php");
        }
    }

    #Adiciona características específicas no header
    public function addHeader()
    {
        if (file_exists(DIRREQ . "app/View/{$this->getDir()}/Header.php")) {
            include(DIRREQ . "app/View/{$this->getDir()}/Header.php");
        }
    }

    #Adiciona características específicas no main
    public function addMain()
    {
        if (file_exists(DIRREQ . "app/View/{$this->getDir()}/Main.php")) {
            include(DIRREQ . "app/View/{$this->getDir()}/Main.php");
        }
    }

    #Adiciona características específicas no footer
    public function addFooter()
    {
        if (file_exists(DIRREQ . "app/View/{$this->getDir()}/Footer.php")) {
            include(DIRREQ . "app/View/{$this->getDir()}/Footer.php");
        }
    }
    /**
     * Alert Mensagens
     *
     * @param string $mensagem
     * @param string $status (alert,success)
     * @return string
     */
    public function alert($mensagem,$status)
    {
        $html = '<div class="container"><div class="'.$status.'">';
        $html .= '<span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span>';
        $html .= '<p>' . $mensagem . '</p>';
        $html .= '</div></div>';
        return $html;
    }
}
