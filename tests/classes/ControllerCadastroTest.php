<?php

namespace Tests;


use PHPUnit\Framework\TestCase;


class ControllerCadastroTest extends TestCase
{
    public function test_cadastro_xml()
    {
        $c1 = $CNPJ = '09066241000884';
        $c2 = $this->cnpj('09066241000884');
        $this->assertEquals($c1, $c2);
    }
    public function cnpj($cnpj): string
    {
        return $cnpj;
    }
}
